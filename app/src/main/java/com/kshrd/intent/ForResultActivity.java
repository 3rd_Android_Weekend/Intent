package com.kshrd.intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kshrd.intent.R;

public class ForResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_result);
    }

    @Override
    public void finish() {
        Intent i = new Intent();
        i.putExtra("msg", "Hello World!");
        setResult(RESULT_OK, i);
        super.finish();
    }
}
