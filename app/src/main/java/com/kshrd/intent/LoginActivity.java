package com.kshrd.intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.kshrd.intent.model.Person;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "ooooo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Bundle extra = getIntent().getExtras();


        String name = extra.getString("name");
        int age = extra.getInt("age", 0);
        Person p = extra.getParcelable("lay");
        ArrayList<Person> people = getIntent().getParcelableArrayListExtra("people");

        for (Person person : people){
            Log.e(TAG, person.toString());
        }

        Toast.makeText(this, "ID : " + p.getId() + ", Name : " + p.getName(), Toast.LENGTH_SHORT).show();

    }
}
