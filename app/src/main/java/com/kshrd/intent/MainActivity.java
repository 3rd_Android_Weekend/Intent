package com.kshrd.intent;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.kshrd.intent.model.Person;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int REQUES_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<Person> people = new ArrayList<>();
        people.add(new Person(100, "Sara"));
        people.add(new Person(101, "Mary"));
        people.add(new Person(102, "Joe"));

        Button btnLoginActivity = (Button) findViewById(R.id.btnLoginActivity);
        btnLoginActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.putExtra("name", "John");
                intent.putExtra("age", 20);
                intent.putExtra("lay", new Person(1, "Lay"));
                intent.putParcelableArrayListExtra("people", people);

                startActivity(intent);
                //finish();
            }
        });

        Button btnGoogle = (Button) findViewById(R.id.btnGoogle);
        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://www.google.com"));

                //Intent chooser = Intent.createChooser(i, "Please Choose");

                if (i.resolveActivity(getPackageManager()) != null) {
                    startActivity(i);
                } else {
                    Toast.makeText(MainActivity.this, "No Intent Handler", Toast.LENGTH_LONG).show();
                }
            }
        });

        findViewById(R.id.btnForResult).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ForResultActivity.class);
                startActivityForResult(i, REQUES_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUES_CODE){
            if (resultCode == RESULT_OK){
                if (data != null){
                    String msg = data.getStringExtra("msg");
                    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
